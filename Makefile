VERSION=v2.3.2_qc

gomod:
	go get chainmaker.org/chainmaker/common/v2@$(VERSION221)
	go get chainmaker.org/chainmaker/protocol/v2@$(VERSION222)
	go mod tidy
